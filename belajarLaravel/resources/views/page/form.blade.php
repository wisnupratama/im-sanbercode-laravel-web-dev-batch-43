<!DOCTYPE html>
<html>
  <head>
    <title>Sign Up</title>
  </head>
  <body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label for="first-name">First Name:</label><br><br>
        <input type="text"  name="first-name"><br><br>
        <label for="last-name">Last Name:</label><br><br>
        <input type="text"  name="last-name"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio"  name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio"  name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio"  name="gender" value="other">
        <label for="other">Other</label><br><br><br>
        <label for="nationality">Nationality:</label><br><br>
        <select id="nationality" name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="singapore">Singapore</option>
        <option value="malaysia">Malaysia</option>
        <option value="Australian">Australian</option>
        </select><br><br>
        <label>Langue Spoken:</label><br><br>
        <input type="checkbox"  name="spoken" value="Bahasa Indonesia">
        <label for="Bahasa Indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox"  name="spoken" value="English">
        <label for="English">English</label><br>
        <input type="checkbox"  name="spoken" value="Other">
        <label for="bio">Bio:</label><br><br>
        <label for="Other">Other</label><br><br>
        <textarea  name="bio" rows="5" cols="30"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
  </body>
</html>