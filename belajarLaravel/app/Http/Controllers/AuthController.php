<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('page.form');
    }

    public function send(Request $request){
        $namaDepan = $request['first-name'];
        $namaBelakang = $request['last-name'];

        return view('page.home', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
