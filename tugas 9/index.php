<?php
require_once('animal.php');
require('Ape.php');
require('Frog.php');

$hewan = new animal("shaun");

echo "Nama Hewan : " . $hewan->name . "<br>";
echo "Jumlah Kaki : " . $hewan->legs . "<br>";
echo "Berdarah dingin : " . $hewan->coldBlooded . "<br><br>";

$kodok = new Frog("buduk");

echo "Nama Hewan : " . $kodok->name . "<br>";
echo "Jumlah Kaki : " . $kodok->legs . "<br>";
echo "Lompatan : " . $kodok->jump() . "<br>";
echo "Berdarah dingin : " . $kodok->coldBlooded . "<br><br>";

$kera = new Ape("Kera Sakti");

echo "Nama Hewan : " . $kera->name . "<br>";
echo "Jumlah Kaki : " . $kera->legs . "<br>";
echo "Berdarah dingin : " . $kera->coldBlooded . "<br>";
echo "Suara Hewan : " . $kera->yell() . "<br>";


?>